<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 8/23/14
 * Time: 5:56 AM
 */

namespace Smorken\Router;

/**
 * Class LegacyRouter
 * @package Smorken\Router
 *
 * Handles routes such as 'login.php', 'about.php', etc to let the router
 * handle legacy style applications
 */
class LegacyRouter extends AbstractRouter implements RouterInterface {

    /**
     * Runs the route
     * Returns the response from $route->run($request)
     * @param string $request Request URI
     * @return mixed|null
     */
    public function dispatch($request)
    {
        $route = $this->getCurrentRoute($request);
        $response = null;
        if ($route) {
            $this->currentRoute = $route;
            $route->runBefores($request);
            $response = $route->run($request);
            $route->runAfters($request, $response);
        }
        return $response;
    }
}