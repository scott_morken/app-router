<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 8/23/14
 * Time: 5:56 AM
 */

namespace Smorken\Router;


use Smorken\Http\Url;

class Route {

    const GET = 'GET';
    const POST = 'POST';
    const PUT = 'PUT';
    const DELETE = 'DELETE';
    const PATCH = 'PATCH';

    public $name;
    public $action;
    public $type;
    public $options = array();

    protected $stack = array('before' => array(), 'after' => array());

    /**
     * @param $name route name
     * @param $type route request method (HTTP verb)
     * @param $action \Closure|array(obj, method)
     * @param array $options
     */
    public function __construct($name, $type, $action, $options = array())
    {
        $this->name = $name;
        $this->type = strtoupper($type);
        $this->action = $action;
        $this->options = $options;
    }

    /**
     * Checks if the request URI matches the route
     * @param Url $urlhandler
     * @param $request_part
     * @return bool
     */
    public function matches(Url $urlhandler, $request_part)
    {
        $comp = $urlhandler->relative($this->name);
        $part_comp = $urlhandler->relative($urlhandler->trimBase($request_part));
        return trim($comp, '/') == trim($part_comp, '/');
    }

    /**
     * Calls the action with the request URI as a parameter
     * @param $request_uri
     * @return mixed
     */
    public function run($request_uri)
    {
        return call_user_func($this->action, $request_uri);
    }

    /**
     * Adds $callable(s) to the before stack
     * @param array|\Closure $callable
     * @param \Pimple\Container $app
     */
    public function before($callable, $app = null)
    {
        $args = func_get_args();
        $add_args = count($args) > 2 ? array_slice($args, 2) : array();
        $this->addCallablesToStack('before', $callable, $app, $add_args);
    }

    /**
     * Adds $callable(s) to the after stack
     * @param array|\Closure $callable
     * @param \Pimple\Container $app
     */
    public function after($callable, $app = null)
    {
        $args = func_get_args();
        $add_args = count($args) > 2 ? array_slice($args, 2) : array();
        $this->addCallablesToStack('after', $callable, $app, $add_args);
    }

    protected function addCallablesToStack($which, $callables, $app, $add_args) {
        if (is_array($callables)) {
            foreach($callables as $c) {
                $this->addToStack($which, $c, $app, $add_args);
            }
        }
        else {
            $this->addToStack($which, $callables, $app, $add_args);
        }
    }

    protected function addToStack($which, $callable, $app, $add_args) {
        if (!is_callable($callable) && $app) {
            if ($app instanceof \Pimple\Container) {
                $callable = $app->raw($callable);
            }
            else {
                $callable = $app->getContainer()->raw($callable);
            }
        }
        $this->stack[$which][] = new Stackable($callable, $app, $add_args);
    }

    /**
     * Runs $options['before'] and any items added with ::before()
     * prior to running the route
     * @param $request
     */
    public function runBefores($request)
    {
        $this->runCallables('before', $request);
    }

    /**
     * Runs $options['after'] and any items added with ::after()
     * after running the route
     * @param $request
     * @param $response
     */
    public function runAfters($request, $response)
    {
        $this->runCallables('after', $request, $response);
    }

    protected function runCallables($key, $request, $response = null)
    {
        $o = (array) array_get($this->options, $key, array());
        $s = (array) array_get($this->stack, $key, array());
        $callables = array_merge($o, $s);
        foreach($callables as $callable) {
            if ($callable instanceof Stackable) {
                $callable->call($request, $response);
            }
            else {
                call_user_func($callable, $request, $response);
            }
        }
    }
} 