<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 8/23/14
 * Time: 5:59 AM
 */

namespace Smorken\Router;


use Smorken\Http\NotFoundHttpException;
use Smorken\Http\Url;

class RouteCollection implements \ArrayAccess {

    /**
     * Array of routes by name
     * @var array
     */
    protected $routes = array();

    /**
     * Array of routes by type and name
     * @var array
     */
    protected $routesByType = array();

    /**
     * Add route to routes and routesByType
     * @param Route $route
     * @return Route
     */
    public function add(Route $route)
    {
        $routename = $route->name;
        $this->routes[$routename] = $route;
        $this->routesByType[$route->type][$routename] = $route;
        return $route;
    }

    /**
     * Create a new Route object and calls ::add
     * @param $name
     * @param $type
     * @param $action
     * @param array $options
     * @return Route
     */
    protected function _add($name, $type, $action, $options = array())
    {
        $r = new Route($name, $type, $action, $options);
        $this->add($r);
        return $r;
    }

    /**
     * Creates a new GET route
     * @param $name
     * @param $closure
     * @param array $options
     * @return Route
     */
    public function get($name, $closure, $options = array())
    {
        return $this->_add($name, Route::GET, $closure, $options);
    }

    /**
     * Creates a new POST route
     * @param $name
     * @param $closure
     * @param array $options
     * @return Route
     */
    public function post($name, $closure, $options = array())
    {
        return $this->_add($name, Route::POST, $closure, $options);
    }

    /**
     * Creates a new PUT route
     * @param $name
     * @param $closure
     * @param array $options
     * @return Route
     */
    public function put($name, $closure, $options = array())
    {
        return $this->_add($name, Route::PUT, $closure, $options);
    }

    /**
     * Creates a new DELETE route
     * @param $name
     * @param $closure
     * @param array $options
     * @return Route
     */
    public function delete($name, $closure, $options = array())
    {
        return $this->_add($name, Route::DELETE, $closure, $options);
    }

    /**
     * Creates a new PATCH route
     * @param $name
     * @param $closure
     * @param array $options
     * @return Route
     */
    public function patch($name, $closure, $options = array())
    {
        return $this->_add($name, Route::PATCH, $closure, $options);
    }

    /**
     * Tries to match the current URI to a route
     * @param $method
     * @param $request_uri
     * @param Url $urlhandler
     * @return Route
     * @throws \Smorken\Http\NotFoundHttpException
     */
    public function match($method, $request_uri, $urlhandler)
    {
        $routes = $this->getByMethod($method);
        $route = $this->check($routes, $request_uri, $urlhandler);
        if ($route) {
            return $route;
        }
        throw new NotFoundHttpException("Route [$method] $request_uri not found.");
    }

    /**
     * Checks the the method routes against the request URI
     * @param Route[] $routes
     * @param $request_uri
     * @param Url $urlhandler
     * @return Route|false
     */
    public function check($routes, $request_uri, $urlhandler)
    {
        foreach($routes as $name => $route) {
            if ($route->matches($urlhandler, $request_uri)) {
                return $route;
            }
        }
        return false;
    }

    /**
     * Returns the routes by method type (HTTP verb)
     * @param $method
     * @return array
     */
    public function getByMethod($method)
    {
        return array_get($this->routesByType, $method, array());
    }

    /**
     * Get the specified route.
     *
     * @param $route
     * @return mixed
     */
    public function fetch($route)
    {
        return (isset($this->routes[$route]) ? $this->routes[$route] : null);
    }

    /**
     * Determine if the given route (exact match) exists.
     *
     * @param  string  $route
     * @return bool
     */
    public function has($route)
    {
        return $this->fetch($route) !== null;
    }

    /**
     * Get all of the route items.
     *
     * @return array
     */
    public function getRoutes()
    {
        return $this->routes;
    }

    /**
     * Determine if the given configuration option exists.
     *
     * @param  string  $key
     * @return bool
     */
    public function offsetExists($key)
    {
        return $this->has($key);
    }

    /**
     * Get a configuration option.
     *
     * @param  string  $key
     * @return mixed
     */
    public function offsetGet($key)
    {
        return $this->fetch($key);
    }

    /**
     * Set a configuration option.
     *
     * @param  string  $key
     * @param  mixed  $value
     * @return void
     */
    public function offsetSet($key, $value)
    {
        $this->routes[$key] = array('type' => Route::GET, 'action' => $value);
    }

    /**
     * Unset a configuration option.
     *
     * @param  string  $key
     * @return void
     */
    public function offsetUnset($key)
    {
        unset($this->routes[$key]);
    }
} 