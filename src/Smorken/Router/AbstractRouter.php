<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 8/23/14
 * Time: 5:56 AM
 */

namespace Smorken\Router;

use Smorken\Http\Url;

class AbstractRouter {

    /**
     * @var RouteCollection
     */
    protected $routes;
    /**
     * @var Route
     */
    protected $currentRoute;
    /**
     * @var \Smorken\Http\Url
     */
    protected $urlHandler;

    /**
     * @param RouteCollection $routes
     * @param Url $url
     */
    public function __construct(RouteCollection $routes, Url $url)
    {
        $this->urlHandler = $url;
        $this->setRoutes($routes);
    }

    /**
     * @param RouteCollection $routes
     */
    public function setRoutes(RouteCollection $routes)
    {
        $this->routes = $routes;
    }

    /**
     * @param $request
     * @return Route
     */
    public function getCurrentRoute($request = null)
    {
        if ($this->currentRoute) {
            return $this->currentRoute;
        }
        $route = null;
        if ($request) {
            $method = $this->urlHandler->getMethod();
            $r_parts = explode('?', $request);
            $route = $this->routes->match($method, $r_parts[0], $this->urlHandler);
        }
        return $route;
    }

}