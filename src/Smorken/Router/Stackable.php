<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 10/2/15
 * Time: 1:23 PM
 */

namespace Smorken\Router;


use Smorken\Application\App;

class Stackable {

    /**
     * @var \Closure
     */
    protected $callable;

    /**
     * @var App|\Pimple\Container
     */
    protected $app;

    /**
     * @var array
     */
    protected $add_args = array();

    public function __construct(\Closure $callable, $app, array $add_args = array())
    {
        $this->callable = $callable;
        $this->app = $app;
        $this->add_args = $add_args;
    }

    public function call($request, $response)
    {
        $params = array($request, $response, $this->app);
        $params = array_merge($params, $this->add_args);
        return call_user_func_array($this->callable, $params);
    }
}