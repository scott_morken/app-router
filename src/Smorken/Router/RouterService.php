<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 8/23/14
 * Time: 6:10 AM
 */

namespace Smorken\Router;


use Smorken\Service\Service;

class RouterService extends Service {

    public function start()
    {
        $this->name = 'router';
    }

    /**
     * Creates the route collection provider and the router provider
     */
    public function load()
    {
        $this->bindRouteCollection();
        $this->bindRouter();

    }

    /**
     * Binds RouteCollection as an instance to 'routes'
     */
    public function bindRouteCollection()
    {
        $this->app->instance('routes', function($c) {
            return new RouteCollection();
        });
    }

    /**
     * Binds LegacyRouter as an instance to 'router'
     */
    public function bindRouter()
    {
        $this->app->instance($this->getName(), function($c) {
            $routes = $c['routes'];
            $r = new LegacyRouter($routes, $c['url']);
            return $r;
        });
    }

} 