<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 8/23/14
 * Time: 5:56 AM
 */

namespace Smorken\Router;


interface RouterInterface {

    public function dispatch($request);

    public function getCurrentRoute($request = null);

} 