## Standalone
~~~~
<?php
include 'vendors/autoload.php';
// create a route collection
$routes = new RouteCollection();
//root routes
$routes->get('', function($request) use ($app) {
    echo "Hello world";
});
$routes->get('index.php', function($request) use ($app) {
    echo "Hello world";
});

$router = new LegacyRouter($routes, new \Smorken\Http\Url($_SERVER));
$router->dispatch($_SERVER['REQUEST_URI']);
~~~~

## Part of simple app

Add a service line to config/app.php services array

```
'Smorken\Router\RouterService'
```

Add the router to the end of the front controller (index.php) before app is returned.

~~~~
$router = $app['router'];
if ($router) {
    require_once __DIR__ . '/../routes/routes.php';
    $router->dispatch($_SERVER['REQUEST_URI']);
}

return $app;
~~~~